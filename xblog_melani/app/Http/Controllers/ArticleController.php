<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Article as Article;
use App\Http\Requests\ArticleRequest;

class ArticleController extends Controller {

    protected $Article;

    public function __construct() {
        $this->Article = new Article();
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$articles = $this->Article->paginate(10);
        return view('articles.index')->with('articles', $articles);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
        return view('articles.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
     * @param ArticleRequest $request
	 * @return Response
	 */
	public function store(ArticleRequest $request)
	{
        $this->Article->create($request->all());
		return redirect('articles');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
