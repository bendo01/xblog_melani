<?php

use Illuminate\Database\Seeder;
use App\Article as Article;


class ArticleTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        Article::unguard();
        Article::truncate();

        /*
        $faker = Faker\Factory::create();
        for($i = 0; $i < 100; $i++) {
            Article::create([
                'title'   => $faker->sentence(),
                'content' => $faker->paragraph(10)
            ]);
        }
        */
	}

}
