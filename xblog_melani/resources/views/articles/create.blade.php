@extends('app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                {!! Form::open(['route'=>'articles.store', 'class' => 'form']) !!}
                    @if($errors->any())
                    <div class="form-group has-error has-feedback">
                    @else
                    <div class="form-group">
                    @endif
                    <div class="form-group">
                        {!! Form::label('title') !!}
                        {!! Form::text('title', null, ['class'=>'form-control', 'placeholder'=>'Judul Anda']) !!}
                        <span class="glyphicon glyphicon-remove form-control-feedback" aria-hidden="true"></span>

                    </div>
                    <div class="form-group">
                        {!! Form::label('content') !!}
                        {!! Form::textarea('content', null, ['class'=>'form-control', 'placeholder'=>'Artikel Anda']) !!}
                    </div>
                    {!!Form::submit('Tambah Artikel', ['class'=>'btn btn-default'])!!}
                {!! Form::close() !!}
            </div>
        </div>

    </div>
@stop