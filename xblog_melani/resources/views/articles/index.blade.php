@extends('app')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <a href="{{ route('articles.create')  }}" class="btn btn-default btn" role="button">
                    Tambah
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Title</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($articles as $article)
                        <tr>
                            <td>{{$article->id}}</td>
                            <td>{{$article->title}}</td>
                            <td>
                                <a href="#" class="btn btn-danger btn" role="button">
                                    Hapus
                                </a>
                                <a href="#" class="btn btn-warning btn" role="button">
                                    Ubah
                                </a>
                                <a href="#" class="btn btn-primary btn" role="button">
                                    Lihat
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php echo $articles->render(); ?>
            </div>
        </div>
    </div>
@stop